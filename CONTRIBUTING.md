# Apporter votre pierre à l'édifice

Nous pouvons tous contribuer à la forme que prendront les après-midis contributions. C'est un processus en cours de création.

Rendez-vous sur la page wiki: https://gitlab.savoirfairelinux.com/emmanuel.milou/pm-contributions/wikis/home pour nous soumettre vos idées! 